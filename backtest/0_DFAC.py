from backtesterRvi import RviBacktest
from backtesterEma import EmaBacktest
from backtesterMomentum import MomBacktest
from backtesterAdvanced import AdvBacktest
from multiprocessing import Process
from datetime import date


tickers = ["DFAC"]
#hianyoznak:
# lefutottak: "AAPL", "TM", "F", "FSLR","GOOG", "AMZN", "SNAP", "BIDU", "META", "GM", "INTC", "ORCL", "HPE", "MSFT", "IBM", "NFLX"
# nem jok: "FB", "TWTR" "HM"
def StartSearch():
    threads = list()
    for tick in tickers:
        print("Starting calculation: {}".format(tick))
        
        bt1 = AdvBacktest(
            tick,  # ticker
            shares=100,  # number of shares (default=1)
            start="2021-01-01",  # start date (default="")
            end="2022-11-05",  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )
        
        x = Process(target=bt1.calculateBest, args=(7, 10, 22, 24, 4, 6, 2, 4, 12, 16, 84, 87, 2, 3))
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()
    #bt1.calculateBest(11, 11, 7, 7, 5, 5, 11, 11, 22, 22, 75, 92, 0, 2, 0, 0)

def StartDayCalc(ticker):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    bt1 = AdvBacktest(
            ticker,  # ticker
            start_money=10000,  # number of shares (default=1)
            start="2022-11-01",  # start date (default="")
            end=d1,  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )

    bt1.calculateOne(8, 23, 5, 3, 14, 85, 3)

StartDayCalc("DFAC")

"""
!!! Igeretes !!! -  IJH
!!! Igeretes !!! -  VB
!!! Igeretes !!! -  BIL
!!! Igeretes !!! -  SHV
!!! Igeretes !!! -  VBR
!!! Igeretes !!! -  DFAC

x = Process(target=bt1.calculateBest, args=(8, 13, 20, 28, 4, 8, 3, 6, 15, 25, 75, 85, 1, 3, 0, 0) )
start="2021-01-01", 
end="2022-11-05",
"""

i=22

# The win ratio in trading is the number of winning trades compared to the total amount of trades
# Profit factor: The ratio between gross profits and gross losses is the profit factor. If you have a strategy that has accumulated 500 in profits and 250 in losses, the profit factor is two.

""" Optimal allocation of capital: https://www.quantifiedstrategies.com/win-rate-trading/"""