from backtesterRvi import RviBacktest
from backtesterEma import EmaBacktest
from backtesterMomentum import MomBacktest
from backtesterAdvanced import AdvBacktest
from multiprocessing import Process
from datetime import date


tickers = ["IJH"]

def StartSearch():
    threads = list()
    for tick in tickers:
        print("Starting calculation: {}".format(tick))
        
        bt1 = AdvBacktest(
            tick,  # ticker
            shares=100,  # number of shares (default=1)
            start="2021-01-01",  # start date (default="")
            end="2022-11-05",  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )
        
        x = Process(target=bt1.calculateBest, args=(8, 8,  24, 25,  7, 7,  6, 7,  11, 12,  84, 85,  3, 3) )
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()

def StartDayCalc(ticker):
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    bt1 = AdvBacktest(
            ticker,  # ticker
            start_money=10000,  # number of shares (default=1)
            start="2022-11-01",  # start date (default="")
            end=d1,  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )

    bt1.calculateOne(8, 24, 7, 6, 12, 84, 3)

StartDayCalc("IJH")

i=22

# The win ratio in trading is the number of winning trades compared to the total amount of trades
# Profit factor: The ratio between gross profits and gross losses is the profit factor. If you have a strategy that has accumulated 500 in profits and 250 in losses, the profit factor is two.

""" Optimal allocation of capital: https://www.quantifiedstrategies.com/win-rate-trading/"""

"""
!!! Igeretes !!! -  IJH
!!! Igeretes !!! -  VB
!!! Igeretes !!! -  BIL
!!! Igeretes !!! -  SHV
!!! Igeretes !!! -  VBR
!!! Igeretes !!! -  DFAC

x = Process(target=bt1.calculateBest, args=(8, 13, 20, 28, 4, 8, 3, 6, 15, 25, 75, 85, 1, 3, 0, 0) )
start="2021-01-01", 
end="2022-11-05",
"""