from backtesterSqueezePro import SqueezePro
from backtesterAdvanced import AdvBacktest
from backtesterEma import EmaBacktest
from datetime import date
from multiprocessing import Process

tickers = ["PG"]
#hianyoznak:
# lefutottak: "AAPL", "TM", "F", "FSLR","GOOG", "AMZN", "SNAP", "BIDU", "META", "GM", "INTC", "ORCL", "HPE", "MSFT", "IBM", "NFLX"
# nem jok: "FB", "TWTR" "HM"
def StartSearch():
    threads = list()
    for tick in tickers:
        print("Starting calculation: {}".format(tick))
        
        bt1 = SqueezePro(
            tick,  # ticker
            start_money=10000,  # number of money at start (default=10000)
            start="2022-01-01",  # start date (default="")
            end="2022-11-05",  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )
        
        x = Process(target=bt1.calculateBest, args=(8,12,  11,12,  8,12,  28,31,  8, 10,  3, 5))
        #x = Process(target=bt1.calculateBest, args=(5, 15,  8, 20,  5, 15,  26,32,  8, 11,  4, 5))
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()
    #bt1.calculateBest(11, 11, 7, 7, 5, 5, 11, 11, 22, 22, 75, 92, 0, 2, 0, 0)

def StartOneCalc():
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    bt1 = SqueezePro(
            tickers[0],  # ticker
            start_money=10000,  # number of shares (default=1)
            start="2022-01-01",  # start date (default="")
            end=d1,  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )

    bt1.calculateOne( 10, 15, 10, 28, 8, 3)

StartOneCalc()

i=22