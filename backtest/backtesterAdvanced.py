from backtesterParent import backtesterParent
from pprint import pprint
from datetime import datetime
from cachedcalc import CachedCalc

class AdvBacktest(backtesterParent):
    ema_period = 10
    ema_close_period = 5
    rsi_period = 10
    atr_period = 10
    rsi_low = 30
    rsi_high = 70
    lower = []
    upper = []
    cachedCalc = 0
    stratName = "Advanced"

    def calculateOne(self, ema, emaclose, rsi, atr, rsi_low, rsi_high, stop):
        self.cachedCalc = CachedCalc(self.getDf(), ema, ema, emaclose, emaclose, rsi, rsi, atr, atr)
        self.ema_period = ema
        self.ema_close_period = emaclose
        self.atr_period = atr
        self.rsi_period = rsi
        self.rsi_low = rsi_low
        self.rsi_high = rsi_high
        self.stop_loss = stop
        self.take_profit = 0
        values = self.run()
        self.printValues(values)
        self.paintLastCalc(self.lower, "Lower", self.upper, "Upper", [], '')
 
    def calculateBest(self, ema_from, ema_to, ema_close_from, ema_close_to, rsi_from, rsi_to, atr_from, atr_to, rsi_low_from, rsi_low_to, rsi_high_from, rsi_high_to, stop_loss_from, stop_loss_to):
        startTime = datetime.now()
        
        self.cachedCalc = CachedCalc(self.getDf(), ema_from, ema_to, ema_close_from, ema_close_to, rsi_from, rsi_to, atr_from, atr_to, rsi_low_from, rsi_low_to, rsi_high_from, rsi_high_to)

        calcNum = (stop_loss_to-stop_loss_from+1)*(ema_to-ema_from+1)*(ema_close_to-ema_close_from+1)*(rsi_to-rsi_from+1)*(rsi_high_to-rsi_high_from+1)*(rsi_low_to-rsi_low_from+1)*(atr_to-atr_from+1)
        print("Number of calculations: ",calcNum) 
        print("Time prediction: %.2f" %(calcNum/82/60), " mins") 

        self.calculateBest_(ema_from, ema_to, ema_close_from, ema_close_to, rsi_from, rsi_to, atr_from, atr_to, rsi_low_from, rsi_low_to, rsi_high_from, rsi_high_to, stop_loss_from, stop_loss_to)
        
        self.sortResultBySharpeRatio()
        self.printResultTopSharpeTest()
        self.saveResultList(['Total Profit','Total Trades','Win Rate','Profit Factor','Maximum Drawdown','Recovery Factor','Riskreward Ratio','Sharpe Ratio','Average Return','Stop Loss Hit','Stop Loss Val', 'Stop Loss %','EMA Period','EMA C Period','RSI Period','ATR Period','RSI Low','RSI High'])

        endTime = datetime.now()
        tdelta = endTime - startTime
        print("Elapsed time: ",tdelta) 
        
    def calculateBest_(self, ema_from, ema_to, ema_close_from, ema_close_to, rsi_from, rsi_to, atr_from, atr_to, rsi_low_from, rsi_low_to, rsi_high_from, rsi_high_to, stop_loss_from, stop_loss_to):
        for sto in range(stop_loss_to-stop_loss_from+1):
            self.stop_loss = (sto+stop_loss_from)*self.df.C.values[10]/100
            for xc in range(ema_to-ema_from+1):
                self.ema_period = xc+ema_from
                print("EMA period - ", xc, " from ", ema_to-ema_from+1)
                for yc in range(rsi_to-rsi_from+1):
                    self.rsi_period = yc+rsi_from
                    for zc in range(rsi_high_to-rsi_high_from+1):
                        self.rsi_high = zc+rsi_high_from
                        for pc in range(rsi_low_to-rsi_low_from+1):
                            self.rsi_low = pc+rsi_low_from
                            for ecps in range(ema_close_to-ema_close_from+1):
                                self.ema_close_period = ecps+ema_close_from
                                for atps in range(atr_to-atr_from+1):
                                    self.atr_period = atps+atr_from
                                    values = self.run()
                                    if (values["Total Profit"] > 10):
                                        values["Stop Loss Val"] = self.stop_loss
                                        values["Stop Loss %"] = sto+stop_loss_from
                                        values["EMA Period"] = self.ema_period
                                        values["EMA C Period"] = self.ema_close_period
                                        values["RSI Period"] = self.rsi_period
                                        values["ATR Period"] = self.atr_period
                                        values["RSI Low"] = self.rsi_low
                                        values["RSI High"] = self.rsi_high
                                        self.saved.append(values)
        
    def printValues(self, values):
        print("----- EMA Period:", self.ema_period, "----- EMA Close Period:", self.ema_close_period, " RSI priod:", self.rsi_period, " ATR Period:", self.atr_period, "-----")
        pprint(values, sort_dicts=False)
        print("-----------------------------------")

    def strategy(self):
        rsi = self.cachedCalc.getRsi(period=self.rsi_period)
        ema = self.cachedCalc.getEma(period=self.ema_period)
        ema_close = self.cachedCalc.getEmaClose(period=self.ema_close_period)
        atr = self.cachedCalc.getAtr(period=self.atr_period)
        self.lower = ema - atr
        self.upper = ema + atr
        self.buy_entry = (rsi < self.rsi_low) & (self.df.C < self.lower)
        self.sell_entry = (rsi > self.rsi_high) & (self.df.C > self.upper)
        self.sell_exit = ema_close > self.df.C
        self.buy_exit = ema_close < self.df.C

"""
!!! Igeretes !!! -  IJH
!!! Igeretes !!! -  VB
!!! Igeretes !!! -  BIL
!!! Igeretes !!! -  SHV
!!! Igeretes !!! -  VBR
!!! Igeretes !!! -  DFAC

x = Process(target=bt1.calculateBest, args=(8, 13, 20, 28, 4, 8, 3, 6, 15, 25, 75, 85, 1, 3, 0, 0) )
start="2021-01-01", 
end="2022-11-05",
"""