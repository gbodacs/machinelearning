from backtesterParent import backtesterParent
from pprint import pprint

class RsiBacktest(backtesterParent):
    rsi_fast = 4
    rsi_slow = 16
    rsi_low = 20 
    rsi_high = 80
    fast_rsi = []
    slow_rsi = []
# Fast and slow RSI needs separate low and high values????

    def calculateOne(self, fast, slow, low, high, stop, take):
        self.rsi_fast = fast
        self.rsi_slow = slow
        self.rsi_low = low
        self.rsi_high = high
        self.stop_loss = stop
        self.take_profit = take
        values = self.run()
        self.printValues(values)
        self.paintLastCalc(self.fast_rsi, "fast_rsi", self.slow_rsi, "slow_rsi")
 
    def calculateBest(self, fast_from, fast_to, slow_from, slow_to, high_from, high_to, low_from, low_to, stop_loss_from, stop_loss_to, take_profit_from, take_profit_to):
        for sto in range(stop_loss_to-stop_loss_from+1):
            self.stop_loss = sto+stop_loss_from
            for prof in range(take_profit_to-take_profit_from+1):
                self.take_profit = prof+take_profit_from
                for xc in range(fast_to-fast_from+1):
                    self.rsi_fast = xc+fast_from
                    for yc in range(slow_to-slow_from+1):
                        self.rsi_slow = yc+slow_from
                        if (self.rsi_slow <= self.rsi_fast):
                            continue
                        for hc in range(high_to-high_from+1):
                            self.rsi_high = hc+high_from
                            for lc in range(low_to-low_from+1):
                                self.rsi_low = lc+low_from
                                values = self.run()
                                if (0 < values["total profit"]):
                                    values["stop loss set"] = self.stop_loss
                                    values["rsi_fast"] = self.rsi_fast
                                    values["rsi_slow"] = self.rsi_slow
                                    values["rsi_high"] = self.rsi_high
                                    values["rsi_low"] = self.rsi_low
                                    self.saved.append(values)
        self.sortResultByTotalProfit()
        self.printResultTopList(5)

    def printValues(self, values):
        print("----- Fast:", self.rsi_fast, " Slow:", self.rsi_slow, " High:", self.rsi_high, " Low:", self.rsi_low, " Stop loss:", self.stop_loss, " TakeProfit: ", self.take_profit, "-----")
        pprint(values, sort_dicts=False)
        print("-----------------------------------")

    def strategy(self):
        self.fast_rsi = self.rsiCalc(period=self.rsi_fast)
        self.slow_rsi = self.rsiCalc(period=self.rsi_slow)

        # golden cross
        self.sell_exit = (self.fast_rsi > self.rsi_low) & ( self.fast_rsi.shift() <= self.rsi_low )

        self.buy_entry = (self.fast_rsi > self.rsi_low) & ( self.fast_rsi.shift() <= self.rsi_low )

        # dead cross
        self.buy_exit = (self.slow_rsi < self.rsi_high) & ( self.slow_rsi.shift() >= self.rsi_high )

        self.sell_entry = (self.slow_rsi < self.rsi_high) & ( self.slow_rsi.shift() >= self.rsi_high )

