from backtesterParent import backtesterParent
from pprint import pprint
from datetime import datetime
from cachedcalc import CachedCalc

class EmaBacktest(backtesterParent):
    ema_short = 6
    ema_long = 18
    fast_ema = []
    slow_ema = []
    stratName = "DoubleEMA"

    def calculateOne(self, short2, long2, stop):
        self.cachedCalc = CachedCalc(self.getDf(), short2, short2, long2, long2, 1,1,1,1,1,1,1,1)
        self.ema_short = short2
        self.ema_long = long2
        self.stop_loss = stop
        self.take_profit = 0
        values = self.run()
        self.printValues(values)
        self.paintLastCalc(self.fast_ema, "Fast EMA", self.slow_ema, "Slow EMA", [], "")
 
    def calculateBest(self, short_from, short_to, long_from, long_to, stop_loss_from, stop_loss_to):
            startTime = datetime.now()
            
            self.cachedCalc = CachedCalc(self.getDf(), short_from, short_to, long_from, long_to, 1,1,1,1,1,1,1,1)

            calcNum = (stop_loss_to-stop_loss_from+1)*(short_to-short_from+1)*(long_to-long_from+1)
            print("Number of calculations: ",calcNum) 
            print("Time prediction: %.2f" %(calcNum/82/60), " mins") 

            self.calculateBest_(short_from, short_to, long_from, long_to, stop_loss_from, stop_loss_to)
            
            self.sortResultBySharpeRatio()
            self.printResultTopSharpeTest()
            self.saveResultList(['Total Profit','Total Trades','Win Rate','Profit Factor','Maximum Drawdown','Recovery Factor','Riskreward Ratio','Sharpe Ratio','Average Return','Stop Loss Hit','Stop Loss Val', 'Stop Loss %','EMA 1 Period','EMA 2 Period'])

            endTime = datetime.now()
            tdelta = endTime - startTime
            print("Elapsed time: ",tdelta)

    def calculateBest_(self, short_from, short_to, long_from, long_to, stop_loss_from, stop_loss_to):
        for sto in range(stop_loss_to-stop_loss_from+1):
            self.stop_loss = (sto+stop_loss_from)*self.df.C.values[10]/100
            for xc in range(short_to-short_from+1):
                print("EMA period - ", xc, " from ", short_to-short_from+1)
                self.ema_short = xc+short_from
                for yc in range(long_to-long_from+1):
                    self.ema_long = yc+long_from
                    if (self.ema_long <= self.ema_short):
                        continue
                    values = self.run()
                    if (10 < values["Total Profit"]):
                        values["Stop Loss Val"] = self.stop_loss
                        values["Stop Loss %"] = sto+stop_loss_from
                        values["EMA 1 Period"] = self.ema_short
                        values["EMA 2 Period"] = self.ema_long
                        self.saved.append(values)

    def printValues(self, values):
        print("----- Short:", self.ema_short, " Long:", self.ema_long, " Stop loss:", self.stop_loss, "-----")
        pprint(values, sort_dicts=False)
        print("-----------------------------------")

    def strategy(self):
        self.fast_ema = self.cachedCalc.getEma(period=self.ema_short)
        self.slow_ema = self.cachedCalc.getEmaClose(period=self.ema_long)

        # golden cross
        self.sell_exit = (self.fast_ema > self.slow_ema) & (
            self.fast_ema.shift() <= self.slow_ema.shift()
        )

        self.buy_entry = (self.fast_ema > self.slow_ema) & (
            self.fast_ema.shift() <= self.slow_ema.shift()
        )

        # dead cross
        self.buy_exit = (self.fast_ema < self.slow_ema) & (
            self.fast_ema.shift() >= self.slow_ema.shift()
        )

        self.sell_entry = (self.fast_ema < self.slow_ema) & (
            self.fast_ema.shift() >= self.slow_ema.shift()
        )

