from backtesterCore import backtesterCore
from pprint import pprint
import csv

class backtesterParent(backtesterCore):
    saved = []
    stratName="default"

    def calculateOne(self, period, stop, take):
        self.period = period
        self.stop_loss = stop
        self.take_profit = take
        values = self.run()
        self.printValues(values)
 
    def sortResultByWinRatio(self):
        self.saved.sort(reverse=True, key=lambda p: p["Win Rate"])

    def sortResultByTotalProfit(self):
        self.saved.sort(reverse=True, key=lambda p: p["Total Profit"])
    
    def sortResultBySharpeRatio(self):
        self.saved.sort(reverse=True, key=lambda p: p['Sharpe Ratio'])

    def printResultTopList(self, num=0):
        print("+++++++++++++++++++++++++++++++++++")
        print("-----------------------------------")
        num2 = num
        if (len(self.saved) < num):
            num2 = len(self.saved)

        for index in range(num2):
            pprint(self.saved[index], sort_dicts=False)
            print("-----------------------------------")
        print("***********************************")

    def printResultTopSharpeTest(self):
        if (len(self.saved) < 1):
            return

        if (self.saved[0]['Sharpe Ratio'] > 0.5):
            print("+++++++++++++++++++++++++++++++++++")
            print("... Egesz jo ... - ", self.ticker)
            if (self.saved[0]['Sharpe Ratio'] > 0.7):
                print("!!! IGERETES !!! - ", self.ticker)
            print("+++++++++++++++++++++++++++++++++++")

    def saveResultList(self, fieldnames):
        # csv header
        #fieldnames = ['name', 'area', 'country_code2', 'country_code3']

        filename = "backtest/calc/y{}+{}--{}-{}.csv".format(self.ticker, self.stratName, self.start, self.end)
        with open(filename, 'w', encoding='UTF8', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.saved)
        print("File saved: ", filename)