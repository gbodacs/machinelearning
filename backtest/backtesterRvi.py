
from typing import Tuple
import pandas as pd
from backtesterParent import backtesterParent
from pprint import pprint

class RviBacktest(backtesterParent):
    period = 5
    rvi = []
    signal = []

    def printValues(self, values):
        print("----- Period:", self.period, "-----")
        pprint(values, sort_dicts=False)
        print("-----------------------------------")

    def calculateOne(self, period2, stop, take):
        self.period = period2
        self.stop_loss = stop
        self.take_profit = take
        values = self.run()
        self.printValues(values)
        self.paintLastCalc(self.rvi, "rvi", self.signal, "signal")

    def calculateBest(self, period_from, period_to, stop_loss_from, stop_loss_to, take_profit_from, take_profit_to):
        for sto in range(stop_loss_to-stop_loss_from+1):
            self.stop_loss = sto+stop_loss_from
            for prof in range(take_profit_to-take_profit_from+1):
                self.take_profit = prof+take_profit_from
                for xc in range(period_to-period_from+1):
                    self.period = xc+period_from
                    values = self.run()
                    if (0 < values["total profit"]):
                        values["stop loss set"] = self.stop_loss
                        values["period"] = self.period
                        self.saved.append(values)

        self.sortResultByTotalProfit()
        self.printResultTopList(5)

    def strategy(self):
        self.rvi, self.signal = self.rviCalc(period=self.period)
        self.sell_exit = self.buy_entry = (self.rvi > self.signal) & (self.rvi.shift() <= self.signal.shift())
        self.buy_exit = self.sell_entry = (self.rvi < self.signal) & (self.rvi.shift() >= self.signal.shift())
