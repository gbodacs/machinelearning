from backtesterParent import backtesterParent
from pprint import pprint
from datetime import datetime
from cachedcalc import CachedCalc
from pandas_ta import squeeze_pro as sq

class SqueezePro(backtesterParent):
    bb_len = 10
    bb_std = 1.5
    kc_len = 10
    kc_scalar_wide = 2
    kc_scalar_normal = 1.5
    kc_scalar_narrow = 1
    mom_length = 18
    mom_smooth = 6
    stratName = "Squeeze_Pro"
    squ = []

    def calculateOne(self, bb_len, bb_std, kc_len, mom_length, mom_smooth, stop):
        self.stop_loss = stop
        self.mom_length = mom_length
        self.mom_smooth = mom_smooth
        self.bb_len = bb_len
        self.bb_std = bb_std/10
        self.kc_len = kc_len
        values = self.run()
        self.printValues(values)
        paint = self.df.C.values[10] + self.squ
        self.paintLastCalc(paint, 'Squeeze', [], '', [], '')
 
    def calculateBest(self, bb_len_from, bb_len_to, bb_std_from, bb_std_to, kc_len_from, kc_len_to, mom_len_from, mom_len_to, mom_smooth_from, mom_smooth_to, stop_loss_from, stop_loss_to):
        startTime = datetime.now()
        calcNum = (mom_len_to-mom_len_from+1)*(mom_smooth_to-mom_smooth_from+1)*(stop_loss_to-stop_loss_from+1)*(bb_len_to-bb_len_from+1)*(bb_std_to-bb_std_from+1)*(kc_len_to-kc_len_from+1)

        print("Number of calculations: ",calcNum) 
        print("Time prediction: %.2f" %(calcNum/65/60), " mins") 

        self.calculateBest_(bb_len_from, bb_len_to, bb_std_from, bb_std_to, kc_len_from, kc_len_to, mom_len_from, mom_len_to, mom_smooth_from, mom_smooth_to, stop_loss_from, stop_loss_to)
        
        self.sortResultBySharpeRatio()
        self.printResultTopSharpeTest()
        self.saveResultList(['Total Profit','Total Trades','Win Rate','Profit Factor','Maximum Drawdown','Recovery Factor','Riskreward Ratio','Sharpe Ratio','Average Return','Stop Loss Hit','Stop Loss Val', 'Stop Loss %', "BB Len", "BB Std", "KC Len", "MOM Len", "MOM Smooth"])

        endTime = datetime.now()
        tdelta = endTime - startTime
        print("Elapsed time: ",tdelta) 
        
    def calculateBest_(self, bb_len_from, bb_len_to, bb_std_from, bb_std_to, kc_len_from, kc_len_to, mom_len_from, mom_len_to, mom_smooth_from, mom_smooth_to, stop_loss_from, stop_loss_to):
        for sto in range(stop_loss_to-stop_loss_from+1):
            self.stop_loss = (sto+stop_loss_from)*self.df.C.values[10]/100
            print("Stop Loss - ", sto, " of ", stop_loss_to-stop_loss_from+1)
            for xc in range(mom_len_to-mom_len_from+1):
                self.mom_length = xc+mom_len_from
                print("   MOM len - ", xc, " of ", mom_len_to-mom_len_from+1)
                for yc in range(mom_smooth_to-mom_smooth_from+1):
                    self.mom_smooth = yc+mom_smooth_from
                    for y2 in range(bb_len_to-bb_len_from+1):
                        self.bb_len = y2+bb_len_from
                        for y3 in range(bb_std_to-bb_std_from+1):
                            self.bb_std = (y3+bb_std_from)/10   # bb_std /10 calculation!!!
                            for yc in range(kc_len_to-kc_len_from+1):
                                self.kc_len = yc+kc_len_from
                                values = self.run()
                                if (values["Total Profit"] > 10):
                                    values["Stop Loss Val"] = self.stop_loss
                                    values["Stop Loss %"] = sto+stop_loss_from
                                    values["BB Len"] = self.bb_len
                                    values["BB Std"] = self.bb_std
                                    values["KC Len"] = self.kc_len
                                    values["MOM Len"] = self.mom_length
                                    values["MOM Smooth"] = self.mom_smooth
                                    self.saved.append(values)
        
    def printValues(self, values):
        print("----- BB Len:", self.bb_len, "BB Std:", self.bb_std, "KC Len:", self.kc_len, "MOM Len:", self.mom_length, "MOM Smooth:", self.mom_smooth, "-----")
        pprint(values, sort_dicts=False)
        print("-----------------------------------")

    def strategy(self):
        ttm = sq(self.getDf()["H"], self.getDf()["L"], self.getDf()["C"], 
                    self.bb_len, 
                    self.bb_std,
                    self.kc_len,
                    self.kc_scalar_wide,
                    self.kc_scalar_normal,
                    self.kc_scalar_narrow,
                    self.mom_length,
                    self.mom_smooth)
        ttm.columns = ["sqzpro", "wide", "normal", "narrow", "off", "no"]
        self.squ = ttm['sqzpro']
        self.buy_entry = (self.squ > self.squ.shift()) & (self.squ.shift() < self.squ.shift().shift()) & (self.squ.shift() < self.squ.shift().shift().shift()) & (self.squ.shift() < self.squ.shift().shift().shift().shift())
        self.buy_exit = (self.squ < self.squ.shift()) & (self.squ.shift() > self.squ.shift().shift()) 
        self.sell_entry = (self.squ < self.squ.shift()) & (self.squ.shift() > self.squ.shift().shift()) & (self.squ.shift() > self.squ.shift().shift().shift()) & (self.squ.shift() > self.squ.shift().shift().shift().shift())
        self.sell_exit = (self.squ > self.squ.shift()) & (self.squ.shift() < self.squ.shift().shift())

        