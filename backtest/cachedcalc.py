from pandas import DataFrame, concat
from typing import Tuple

class CachedCalc:
    emaList = []
    emaCloseList = []
    rsiList = []
    atrList = []
    df = 0

    def __init__(self, df, ema_from, ema_to, ema_close_from, ema_close_to, rsi_from, rsi_to, atr_from, atr_to):
        self.df = df
        for xc in range(ema_to+1):
            self.emaList.append(0)

        for ic in range(ema_close_to+1):
            self.emaCloseList.append(0)
            
        for yc in range(rsi_to+1):
            self.rsiList.append(0)

        for ac in range(atr_to+1):
            self.atrList.append(0)

        for xc in range(ema_to-ema_from+1):
            self.emaList[xc+ema_from] = self.emaCalc(period=xc+ema_from)

        for ic in range(ema_close_to-ema_close_from+1):
            self.emaCloseList[ic+ema_close_from] = self.emaCalc(period=ic+ema_close_from)
            
        for yc in range(rsi_to-rsi_from+1):
            self.rsiList[yc+rsi_from] = self.rsiCalc(period=yc+rsi_from)

        for ac in range(atr_to-atr_from+1):
            self.atrList[ac+atr_from] = self.atrCalc(period=ac+atr_from)

    def getEma(self, period):
        return self.emaList[period]

    def getEmaClose(self, period):
        return self.emaCloseList[period]

    def getRsi(self, period):
        return self.rsiList[period]

    def getAtr(self, period):
        return self.atrList[period]


    def smaCalc(self, *, period: int, price: str = "C") -> DataFrame:
        return self.df[price].rolling(period).mean()

    def emaCalc(self, *, period: int, price: str = "C") -> DataFrame:
        return self.df[price].ewm(span=period).mean()

    def bbandsCalc(self, *, period: int = 20, band: int = 2, price: str = "C") -> Tuple[DataFrame, DataFrame, DataFrame]:
        std = self.df[price].rolling(period).std()
        mean = self.df[price].rolling(period).mean()
        return mean + (std * band), mean, mean - (std * band)

    def macdCalc(
        self,
        *,
        fast_period: int = 12,
        slow_period: int = 26,
        signal_period: int = 9,
        price: str = "C",
    ) -> Tuple[DataFrame, DataFrame]:
        macd = (
            self.df[price].ewm(span=fast_period).mean()
            - self.df[price].ewm(span=slow_period).mean()
        )
        signal = macd.ewm(span=signal_period).mean()
        return macd, signal

    def stochCalc(
        self, *, k_period: int = 5, d_period: int = 3
    ) -> Tuple[DataFrame, DataFrame]:
        k = (
            (self.df.C - self.df.L.rolling(k_period).min())
            / (self.df.H.rolling(k_period).max() - self.df.L.rolling(k_period).min())
            * 100
        )
        d = k.rolling(d_period).mean()
        return k, d

    def rsiCalc(self, *, period: int = 14, price: str = "C") -> DataFrame:
        return 100 - 100 / (
            1
            - self.df[price].diff().clip(lower=0).rolling(period).mean()
            / self.df[price].diff().clip(upper=0).rolling(period).mean()
        )

    def atrCalc(self, *, period: int = 14, price: str = "C") -> DataFrame:
        a = (self.df.H - self.df.L).abs()
        b = (self.df.H - self.df[price].shift()).abs()
        c = (self.df.L - self.df[price].shift()).abs()

        df = concat([a, b, c], axis=1).max(axis=1)
        return df.ewm(span=period).mean()

    def rviCalc(self, *, period: int = 10, price: str = "C") -> Tuple[DataFrame, DataFrame]:
        co = self.df.C - self.df.O
        n = (co + 2 * co.shift(1) + 2 * co.shift(2) + co.shift(3)) / 6
        hl = self.df.H - self.df.L
        d = (hl + 2 * hl.shift(1) + 2 * hl.shift(2) + hl.shift(3)) / 6
        rvi = n.rolling(period).mean() / d.rolling(period).mean()
        signal = (rvi + 2 * rvi.shift(1) + 2 * rvi.shift(2) + rvi.shift(3)) / 6
        return rvi, signal
