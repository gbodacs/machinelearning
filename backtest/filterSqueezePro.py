import glob
import pandas as pd
from pprint import pprint

#'Total Profit','Total Trades','Win Rate','Profit Factor','Maximum Drawdown','Recovery Factor','Riskreward Ratio','Sharpe Ratio','Average Return','Stop Loss Hit','Stop Loss Val', 'Stop Loss %', "BB Len", "BB Std", "KC Len", "MOM Len", "MOM Smooth"

StartPath = "/home/gabor/Desktop/Squeeze_results/0_Igeretes/kivalogatva/"
SharpeMin = 0.7
WinRateMin = 70
TotalProfitMin = 4000
StopLossMin = 1

def SearchAndFilterFiles(path):
  fileList = [f for f in glob.glob(path+"*.csv")]
  for file in fileList:
    df = pd.read_csv(file, parse_dates=True)
    df2 = df[df['Sharpe Ratio']>SharpeMin]
    df3 = df2[df2['Win Rate']>WinRateMin]
    df30 = df3[df3['Total Profit']>TotalProfitMin]
    df4 = df30[df30['Stop Loss Hit']>StopLossMin]
    if (df4.size > 0):
      print("...Found (", df4.size,") in the file:", file)
      #pprint(df4[0], sort_dicts=False)
    else:
      i = 22
    

print(" ")
print("Program start")
print("-------------")
SearchAndFilterFiles(StartPath)
print("-------------")
print("Finish")
