from backtesterSqueezePro import SqueezePro
from backtesterAdvanced import AdvBacktest
from backtesterAdvanced2 import AdvBacktest2

from datetime import date
from multiprocessing import Process

tickers = ["XLP", "XLI", "XLU", "XLY", "XLE", "XLF"]

def StartSearch():
    threads = list()
    for tick in tickers:
        print("Starting calculation of ticker: {}".format(tick))
        
        bt1 = AdvBacktest2(
            tick,  # ticker
            start_money=10000,  # number of money at start (default=10000)dddd
            start="2022-01-01",  # start date (default="")
            end="2022-11-05",  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )
        
        x = Process(target=bt1.calculateBest, args=(5, 11,  17, 28,  5, 8,  4, 8,   3, 19,   15, 22,  78, 85,  1, 1) )
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        thread.join()
    

def StartOneTest():
    today = date.today()
    d1 = today.strftime("%Y-%m-%d")
    bt1 = SqueezePro(
            tickers[0],  # ticker
            start_money=10000,  # number of shares (default=1)
            start="2022-01-01",  # start date (default="")
            end=d1,  # end date (default="")
            data_dir="adv2",  # data directory (default=.)
        )

    bt1.calculateOne( 10, 15, 10, 28, 8, 3)

StartSearch()

i=22

#AMZN 
    #1-EMA ema_close nelkul:
        # 3 éves tavon ez a legjobb?
            #$10114.749	21pcs	90.476%	9.618	1005	10.064	1.012	0.851	333.185	0	0	0	0	11	xx 10	7	21	82
        # 4 eves tavon legyengul, de meg igy is eleg jo!
    #2-EMA ema_close hozzaadva
        # 2 éves tavlatban ez a legjobb:? 1pcs = $93
        #$14847.854	26pcs	92.308%	32.931	284	    52.281	2.744	1.022	399.284	0	0	0	0	13	32	7	4	22	85

#best:                                                              ema_period     rsi_period      atr_period      rsi_low     rsi_high
#SNAP  $3294.499	20pcs	85%	    9.591	259	12.72	1.692	0.963	637.438	0	0	0	0	5	xx 6	9	20	79
    #nem tünik jonak a strategia
#HPE   $337.5	    11pcs	81.818%	10.926	31	10.887	2.428	0.716	251.574	0	0	0	0	11	xx 12	5	21	82
    #4 eves tavlatban nem hoz profitot

# impossible:
#    (1, 15, 1, 15, 1, 15, 2, 35, 65, 98, 0, 8, 0, 0) 



# RVI !
#bt.calculateBest(period_from=1, period_to=9, stop_loss_from=1, stop_loss_to=10, take_profit_from=0, take_profit_to=0 )
#bt.calculateOne(7, 2, 0)
#bt.calculateOne(5, 14)
#bt.calculateOne(6, 8)


# The win ratio in trading is the number of winning trades compared to the total amount of trades
# Profit factor: The ratio between gross profits and gross losses is the profit factor. If you have a strategy that has accumulated 500 in profits and 250 in losses, the profit factor is two.

""" Optimal allocation of capital: https://www.quantifiedstrategies.com/win-rate-trading/"""