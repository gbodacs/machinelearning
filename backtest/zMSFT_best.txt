total profit	total trades	win rate	profit factor	maximum drawdown	recovery factor	riskreward ratio	sharpe ratio	average return	stop loss hit	take profit hit	stop loss set	take profit set	ema_period	rsi_period	atr_period	rsi_low	rsi_high

2153.998	8	87.5	4.113	692.001	3.113	0.588	0.549	105.8	0	0	0	0	4	7	6	22	79
2153.998	8	87.5	4.113	692.001	3.113	0.588	0.549	105.8	0	0	0	0	4	7	6	22	80

15161.003	34	76.471	2.791	3561	4.258	0.859	0.432	179.551	0	0	0	0	11	5	9	22	82
15139.003	34	76.471	2.788	3561	4.251	0.858	0.431	179.273	0	0	0	0	11	5	10	22	82
15139.003	34	76.471	2.788	3561	4.251	0.858	0.431	179.273	0	0	0	0	11	5	11	22	82

4év:
 'total profit': 19550.002,
 'total trades': 71,
 'win rate': 76.056,
 'profit factor': 2.59,
 'maximum drawdown': 2959.999,
 'recovery factor': 6.605,
 'riskreward ratio': 0.815,
 'sharpe ratio': 0.385,
 'average return': 140.367,
 'stop loss hit': 0,
 'take profit hit': 0,
 'stop loss set': 0,
 'take profit set': 0,
 'ema_period': 11,
 'rsi_period': 5,
 'atr_period': 11,
 'rsi_low': 22,
 'rsi_high': 86