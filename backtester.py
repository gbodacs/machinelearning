# https://towardsdatascience.com/backtest-your-trading-strategy-with-only-3-lines-of-python-3859b4a4ab44

# Relative Strength Index (RSI) 	            rsi 	rsi_period, rsi_upper, rsi_lower
# Simple moving average crossover (SMAC) 	    smac 	fast_period, slow_period
# Exponential moving average crossover (EMAC) 	emac 	fast_period, slow_period
# Moving Average Convergence Divergence (MACD) 	macd 	fast_perod, slow_upper, signal_period, sma_period, dir_period
# Bollinger Bands 	                            bbands 	period, devfactor
# Buy and Hold 	                                buynhold 	N/A
# Sentiment Strategy 	                        sentiment 	keyword , page_nums, senti
# Custom Prediction Strategy                	custom 	upper_limit, lower_limit, custom_column
# Custom Ternary Strategy 	                    ternary 	buy_int, sell_int, custom_column

#from fastquant import backtest
from fastquant import get_stock_data


df = get_stock_data("IBM", "2018-01-01", "2021-02-01")
backtest('smac', df, fast_period=range(8, 9, 10, 11, 12, 13, 14, 15), slow_period=range(35,36,37,38,39,40,41,42,43,44,45) )
backtest('rsi', df, rsi_period=range(11,12,13,14,15,16,17,18), rsi_upper=range(70,75,80, 85, 90), rsi_lower=range(30, 25, 20, 15, 10))

i=12



# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------

from fastquant import get_yahoo_data, get_bt_news_sentiment
data = get_yahoo_data("TSLA", "2020-01-01", "2020-07-04")
sentiments = get_bt_news_sentiment(keyword="tesla", page_nums=3)
backtest("sentiment", data, sentiments=sentiments, senti=0.2)

# Starting Portfolio Value: 100000.00
# Final Portfolio Value: 313198.37
# Note: Unfortunately, you can't recreate this scenario due to inconsistencies in the dates and sentiments that is scraped by get_bt_news_sentiment. In order to have a quickstart with News Sentiment Strategy you need to make the dates consistent with the sentiments that you are scraping.

from fastquant import get_yahoo_data, get_bt_news_sentiment
from datetime import datetime, timedelta

# we get the current date and delta time of 30 days
current_date = datetime.now().strftime("%Y-%m-%d")
delta_date = (datetime.now() - timedelta(30)).strftime("%Y-%m-%d")
data = get_yahoo_data("TSLA", delta_date, current_date)
sentiments = get_bt_news_sentiment(keyword="tesla", page_nums=3)
backtest("sentiment", data, sentiments=sentiments, senti=0.2)

# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------

df = get_stock_data("JFC", "2018-01-01", "2019-01-01")

# Utilize single set of parameters
strats = { 
    "smac": {"fast_period": 35, "slow_period": 50}, 
    "rsi": {"rsi_lower": 30, "rsi_upper": 70} 
} 
res = backtest("multi", df, strats=strats)
res.shape
# (1, 16)


# Utilize auto grid search
strats_opt = { 
    "smac": {"fast_period": 35, "slow_period": [40, 50]}, 
    "rsi": {"rsi_lower": [15, 30], "rsi_upper": 70} 
} 

res_opt = backtest("multi", df, strats=strats_opt)
res_opt.shape
# (4, 16)