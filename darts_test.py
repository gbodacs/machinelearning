import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from darts import TimeSeries
from darts.metrics import mape
from darts.models import TFTModel
from darts.dataprocessing.transformers import (Scaler, MissingValuesFiller)
from darts.utils.likelihood_models import QuantileRegression
from darts.models import KalmanFilter
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

import torch
import pandas_ta as pta

filler = MissingValuesFiller()
kf = KalmanFilter(dim_x=1)

vi1 = TimeSeries.from_csv("./data/VIX_daily.csv", "Date", ["Open", "Close"], True, "D")
vi1 = vi1.add_holidays("US")
vi1 = (vi1["Close"]+vi1["Open"])/2
vi2 = filler.transform(vi1, method="quadratic")

df0 = TimeSeries.from_csv("./data/SPY_daily.csv", "Date", ["Close", "Open", "Low", "High"], True, "D")
df0 = df0.add_holidays("US")
df1 = (df0["Close"]+df0["Open"]+df0["High"]+df0["Low"])/4
df2 = filler.transform(df1, method="quadratic")

kf.fit(vi2)
vi2 = kf.filter(vi2)

kf.fit(df2, vi2)
df2 = kf.filter(df2, vi2)

# pta.rsi(df1['Close'], length = 14)

# Create training and validation sets:
training_cutoff = pd.Timestamp("20201201")

train, test = df2.split_after(training_cutoff)
trainVIX, testVIX = vi2.split_after(training_cutoff)

# normalize
transformer1 = Scaler()
train_transformed = transformer1.fit_transform(train)
test_transformed = transformer1.transform(test)

transformer2 = Scaler()
vi_transformed = transformer2.fit_transform(vi2)
vi_transformed = 1-vi_transformed["Close"]

''' best_theta_model = TFTModel(
        add_encoders={"cyclic": {"future": ["hour"]}},
        optimizer_kwargs={"lr": 1e-3},
        
        save_checkpoints=True,
        work_dir="./models/",

        force_reset=True,
        pl_trainer_kwargs={"accelerator": "gpu","devices": [0]},
    ) '''
quantiles = [
    0.01,
    0.05,
    0.1,
    0.15,
    0.2,
    0.25,
    0.3,
    0.4,
    0.5,
    0.6,
    0.7,
    0.75,
    0.8,
    0.85,
    0.9,
    0.95,
    0.99,
]

my_stopper = EarlyStopping(
    monitor="train_loss",
    patience=5,
    min_delta=0.05,
    mode='min',
)

input_chunk_length = 50
forecast_horizon = 10
best_model = TFTModel(
    input_chunk_length=input_chunk_length,
    output_chunk_length=forecast_horizon,
    hidden_size=64,
    lstm_layers=1,
    num_attention_heads=4,
    dropout=0.1,
    batch_size=16,
    n_epochs=30,
    add_relative_index=True,
    add_encoders=None,
    likelihood=QuantileRegression(
        quantiles=quantiles
    ),  # QuantileRegression is set per default
    # loss_fn=MSELoss(),
    random_state=13157346,
    #pl_trainer_kwargs={"accelerator": "gpu","devices": [0]}
    pl_trainer_kwargs={"callbacks": [my_stopper]}
)

best_model.fit(
        train_transformed,
        #past_covariates=vi_transformed,
        #future_covariates=vi_transformed,
        verbose=True
    )


prediction = best_model.predict(
        len(test), 

        #past_covariates=vi_transformed,
        #future_covariates=vi_transformed,
    )

print( "The MAPE is: {:.2f}".format(mape(test_transformed, prediction)) )


train_transformed.plot(label="train")
test_transformed.plot(label="true")
vi_transformed.plot(label="VIX")
prediction.plot(label="prediction")

# historical_fcast_theta = best_theta_model.historical_forecasts(
#     df2, start=0.6, forecast_horizon=3, verbose=True
# )

# df2.plot(label="data")
# historical_fcast_theta.plot(label="backtest 3-months ahead forecast (Theta)")
# print("MAPE = {:.2f}%".format(mape(historical_fcast_theta, df)))

# def eval_model(model):
#     model.fit(train)
#     forecast = model.predict(len(test))
#     forecast.plot()
#     print("model {} obtains MAPE: {:.2f}%".format(model, mape(test, forecast)))


# eval_model(Theta())


# # df.plot(label="actual")
# #naive_forecast.plot(label="naive forecast (K=12)")
# train.plot(label="train")
# test.plot(label="test")

i=22