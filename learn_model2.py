import numpy
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import random
import sys, getopt
from keras.optimizers import SGD
from keras.models import Sequential
from keras.models import load_model
from keras.callbacks import Callback
from keras.layers import Dense, Conv1D, MaxPooling1D, Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
#
# learn.py: learn the whole database and save the model
# test.py: test the model with the learned database
#	- use some params to predict not only one element in the future
# predict.py: predict the future using the saved model and a database
#
# Implement callback function to stop training
# when accuracy reaches ACCURACY_THRESHOLD
ACCURACY_THRESHOLD = 0.95

class myCallback(Callback):
	def on_epoch_end(self, epoch, logs={}):
		if(logs.get('accuracy') > ACCURACY_THRESHOLD):
			print("\nReached %2.2f%% accuracy, so stopping training!!" %(ACCURACY_THRESHOLD*100))
			self.model.stop_training = True

# Instantiate a callback object
callbacks = myCallback()

# convert an array of values into a dataset matrix
def create_dataset_learn(datasetIn, datasetOut, look_back=1):
	dataX, dataY = [], []
	if len(datasetIn) != len(datasetOut):
		return numpy.array(dataX), numpy.array(dataY)
		
	for i in range(len(datasetIn)-look_back-1):
		in12 = datasetIn[i:(i+look_back)]
		dataX.append(in12)
		out12 = datasetOut[i + look_back]
		dataY.append(out12)
	return numpy.array(dataX), numpy.array(dataY)

# def create_dataset_learn(dataset, look_back=1):
# 	dataX, dataY = [], []
# 	for i in range(len(dataset)-look_back-1):
# 		dataX.append([dataset[i, 0]])
# 		dataY.append(dataset[i + look_back, 0])
# 	return numpy.array(dataX), numpy.array(dataY)

inputfile = sys.argv[1]
print ('Input CSV file is "', inputfile, '"')

# fix random seed for reproducibility
numpy.random.seed(7)

# load the dataset
dataframeIn = read_csv(inputfile, usecols=[1,2,3,4])
datasetIn = dataframeIn.values
datasetIn = datasetIn.astype('float32')
scalerIn = MinMaxScaler(feature_range=(0, 0.9))
datasetIn = scalerIn.fit_transform(datasetIn)

dataframeOut = read_csv(inputfile, usecols=[10,11,12,13,14]) 
datasetOut = dataframeOut.values
datasetOut = datasetOut.astype('float32')

epochNumber = 10
train_size = int(len(dataframeIn) * 0.80)
test_size = len(dataframeIn)-train_size

# load test sets
trainIn , testIn  = datasetIn[:train_size,:], datasetIn[train_size:train_size+test_size,:]
trainOut, testOut = datasetOut[:train_size,:], datasetOut[train_size:train_size+test_size,:]

# reshape into X=t and Y=t+1
look_back = 40

trainX, trainY = create_dataset_learn(trainIn, trainOut, look_back)
testX, testY = create_dataset_learn(testIn, testOut, look_back)

# reshape input to be [samples, time steps, features]
#xtrainX = numpy.rot90(trainX)
#testX = numpy.rot90(testX)

trainY = numpy.reshape(trainY, (trainY.shape[0], 1, trainY.shape[1]))
testY = numpy.reshape(testY, (testY.shape[0], 1, testY.shape[1]))

drop_out_rate = 0.5

# create and fit the LSTM network
model = Sequential()
model.add(Conv1D(filters=10, kernel_size=20, padding='valid', activation='relu', input_shape=(look_back, 4)))
model.add(MaxPooling1D(1))

model.add(Dense(512, activation='relu'))
model.add(Dropout(drop_out_rate))
model.add(Dense(128, activation='relu'))
model.add(Dropout(drop_out_rate))
model.add(Dense(5, activation='softmax')) 
model.compile(loss='binary_crossentropy', optimizer="adam", metrics=['accuracy'])
model.fit(trainX, trainY, epochs=epochNumber, batch_size=16, callbacks=[callbacks])

#model.add(LSTM(40, activation='relu', return_sequences=True,  input_shape=(look_back, 4)))
#model.add(Dropout(0.3)) 
#model.add(Dense(5, activation='softmax')) 
#model.compile(loss='binary_crossentropy', optimizer="adam", metrics=['accuracy'])
#model.fit(trainX, trainY, epochs=epochNumber, batch_size=16, callbacks=[callbacks])

# save model
#model.save(inputfile + "_model")

# evaluate model with testdata
model.evaluate(testX, testY)

# make predictions and evaluate it
trainPredict = model.predict(trainX)
testPredict = model.predict(testX)

print("End of program")

# invert predictions
#trainPredict = scaler.inverse_transform(trainPredict)
#trainY = scaler.inverse_transform([trainY])
#testPredict = scaler.inverse_transform(testPredict)
#testY = scaler.inverse_transform([testY])

# calculate root mean squared error
#trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:,0]))
#print('Train Score: %.2f RMSE' % (trainScore))
#testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))
#print('Test Score: %.2f RMSE' % (testScore))

# shift train predictions for plotting
#trainPredictPlot = numpy.empty_like(dataset)
#trainPredictPlot[:, :] = numpy.nan
#trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict

# shift test predictions for plotting
#testPredictPlot = numpy.empty_like(dataset)
#testPredictPlot[:, :] = numpy.nan
#testPredictPlot[len(trainPredict)+(look_back*2)+1:len(testPredictPlot)-1, :] = testPredict

# plot baseline and predictions
#plt.plot(scaler.inverse_transform(dataset))
#plt.plot(trainPredictPlot)
#plt.plot(testPredictPlot)
#plt.show()