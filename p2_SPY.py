
import pandas as pd
from fbprophet import Prophet
from fbprophet.plot import plot_plotly, plot_components_plotly
from matplotlib import pyplot

import numpy
from numpy import array
from numpy import hstack
import matplotlib.pyplot as plt
from pandas import read_csv
import datetime
import glob, os
import math
import random
import sys, getopt
# from keras.optimizers import SGD
# from keras.models import Sequential
# from keras.models import load_model
# from keras.callbacks import Callback
# from keras.layers import Dense
# from keras.layers import Dropout
# from keras.layers import LSTM
# from keras.layers import RepeatVector
# from keras.layers import TimeDistributed
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_log_error

class DataManager:
    def __init__(self):
        self.MSE = list()
        self.MAE = list()
        self.SLE = list()

    def AddMSE(self, data, cycle):
        self.MSE.append([data, cycle])

    def AddMAE(self, data, cycle):
        self.MAE.append([data, cycle])

    def AddSLE(self, data, cycle):
        self.SLE.append([data, cycle])

    def Reset(self):
        self.MSE.clear()
        self.MAE.clear()
        self.SLE.clear()

    def Write(self, fileName, lines):
        if (fileName == ""):
            for x in lines:
                print(x, end="")
        else:
            f = open(fileName+".txt", "w")
            for x in lines:
                f.write(x)
            f.close()

    def FinalizeAndPrint(self, fileName):
        self.MSE.sort(key=lambda x:x[0])
        self.MAE.sort(key=lambda x:x[0])
        self.SLE.sort(key=lambda x:x[0])

        lines = list()
        numToWrite = 10
        
        lines.append("----------------------------\n")
        r_mse = 0
        if (len(self.MSE)>numToWrite):
            r_mse = numToWrite
        else:
            r_mse = len(self.MSE)
        for x in range(r_mse):
            lines.append(f"{x+1}. MSE - {self.MSE[x][0]} at cycles: {self.MSE[x][1]}\n")

        lines.append("----------------------------\n")
        r_mae = 0
        if (len(self.MAE)>numToWrite):
            r_mae = numToWrite
        else:
            r_mae = len(self.MAE)
        for x in range(r_mae):
            lines.append(f"{x+1}. MAE - {self.MAE[x][0]} at cycles: {self.MAE[x][1]}\n")

        lines.append("----------------------------\n")
        r_sle = 0
        if (len(self.SLE)>numToWrite):
            r_sle = numToWrite
        else:
            r_sle = len(self.SLE)
        for x in range(r_sle):
            lines.append(f"{x+1}. SLE - {self.SLE[x][0]} at cycles: {self.SLE[x][1]}\n")
        lines.append("----------------------------\n")

        self.Write(fileName, lines)

def normal_analysis(data, forecast_plot = False):
    training = []
    testing = []
    if (len(data) > 5500 ):
        training = data[-5500:-20].iloc[:-1,]
        testing = data[-20:]
    else:
        training = data[0:-200].iloc[:-1,]
        testing = data[-200:]
    predict_period = 1000#len(pd.date_range(split_date,max(data.index)))
    df = training.reset_index()
    df.columns = ['index','ds','y']
    training.columns = ['ds','y']
    testing.columns = ['ds','y']
    
    m = Prophet()

    m.fit(df)
    future = m.make_future_dataframe(periods=predict_period)
    forecast = m.predict(future)
    if forecast_plot:
        m.plot(forecast)

        testDate = testing.values[:,0] #date
        testValue = testing.values[:,1] #value

        conv_dates = []
        for i in range(len(testDate)):
            date1 = datetime.datetime.strptime(testing.values[i,0], '%Y-%m-%d').date()
            conv_dates = numpy.append(conv_dates, date1)
        plt.plot(conv_dates, testValue, '.', color='#ff3333', alpha=0.6)

        plt.xlabel('Date', fontsize=12, fontweight='bold', color='gray')
        plt.ylabel('Price', fontsize=12, fontweight='bold', color='gray')
        plt.show()
    
    temp = forecast['yhat']
    Mse = mean_squared_error(testing["y"], temp[0:len(testing["y"])] )
    Mae = mean_absolute_error(testing["y"], temp[0:len(testing["y"])] )
    Sle = mean_squared_log_error(testing["y"], temp[0:len(testing["y"])] )
    
    #dm.AddMSE(Mse, cycle)
    #dm.AddMAE(Mae, cycle)
    #dm.AddSLE(Sle, cycle)

    #print(f"MSE: {Mse} - MAE: {Mae} at cycle: {cycle}")

    return 0

def RunOneTest(): 
    fileName = "./data/IBM_daily.csv"
    df = pd.read_csv(fileName, usecols=[0,4])
    print("One Test  - Processing: "+fileName)
    normal_analysis(df, forecast_plot=True) 
    dm.FinalizeAndPrint("")

dm = DataManager()
RunOneTest()
print("Done.")
