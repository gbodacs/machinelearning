# Data processing
import pandas as pd
import numpy as np
# Get time series data
import yfinance as yf
# Prophet model for time series forecast
from prophet import Prophet
# Visualization
import seaborn as sns
import matplotlib.pyplot as plt
# Multi-processing
from multiprocessing import Pool, cpu_count
# Spark
from pyspark.sql.types import *
from pyspark.sql.functions import pandas_udf, PandasUDFType
# Process bar
from tqdm import tqdm
# Tracking time
from time import time
#-------------------------------------------------------------#
# Step 2: Pull Data
#-------------------------------------------------------------#
# Data start date
start_date = '2020-01-02'
# Data end date
end_date = '2022-11-01' # yfinance excludes the end date, so we need to add one day to the last day of data
# Download data
ticker_list = ['SPY', 'GOOG', 'ORCL', 'MSFT', 'AMZN']
data = yf.download(ticker_list, start=start_date, end=end_date)[['Close']]
# Drop the top level column name
data.columns = data.columns.droplevel()
# Take a look at the data
data.head()
# Visualize data using seaborn
sns.set(rc={'figure.figsize':(12,8)})
sns.lineplot(x=data.index, y=data['SPY'])
sns.lineplot(x=data.index, y=data['AMZN'])
sns.lineplot(x=data.index, y=data['GOOG'])
sns.lineplot(x=data.index, y=data['MSFT'])
sns.lineplot(x=data.index, y=data['ORCL'])
plt.legend(labels = ['SPY', 'Amazon', 'Google', 'Microsoft', 'Oracle'])
# Data information
data.info()
#-------------------------------------------------------------#
# Step 3: Data Processing
#-------------------------------------------------------------#
# Release Date from the index
data = data.reset_index()
# Change data from the wide format to the long format
df = pd.melt(data, id_vars='Date', value_vars=['AMZN', 'SPY', 'GOOG', 'MSFT', 'ORCL'])
df.columns = ['ds', 'ticker', 'y']
df.head()
# Check the dataset information
df.info()
# Group the data by ticker
groups_by_ticker = df.groupby('ticker')
# Check the groups in the dataframe
groups_by_ticker.groups.keys()
#-------------------------------------------------------------#
# Step 4: Define Function
#-------------------------------------------------------------#
def train_and_forecast(group):
  # Initiate the model
  m = Prophet()
  # m.add_regressor('add1')
  # m.add_regressor('add2')
  # Fit the model
  m.fit(group)
  # Make predictions
  future = m.make_future_dataframe(periods=15)
  forecast = m.predict(future)[['ds', 'yhat', 'yhat_lower', 'yhat_upper']]
  forecast['ticker'] = group['ticker'].iloc[0]
  
  # Return the forecasted results
  return forecast[['ds', 'ticker', 'yhat', 'yhat_upper', 'yhat_lower']]  
#-------------------------------------------------------------#
# Step 5: Multiple Time Series Forecast Using For-Loop
#-------------------------------------------------------------#
# Start time
start_time = time()
# Create an empty dataframe
for_loop_forecast = pd.DataFrame()
# Loop through each ticker
for ticker in ticker_list:
  # Get the data for the ticker
  group = groups_by_ticker.get_group(ticker)  
  # Make forecast
  forecast = train_and_forecast(group)
  # Add the forecast results to the dataframe
  for_loop_forecast = pd.concat((for_loop_forecast, forecast))
# Print processing time
print('The time used for the for-loop forecast is ', time()-start_time)
# Take a look at the data
for_loop_forecast.head()
#-------------------------------------------------------------#
# Step 6: Multiple Time Series Forecast Using Multi-Processing
#-------------------------------------------------------------#
# Start time
start_time = time()
# Get time series data for each ticker and save in a list
series = [groups_by_ticker.get_group(ticker) for ticker in ticker_list]
# Create a pool process with the number of worker processes being the number of CPUs
p = Pool(cpu_count())
# Make predictions for each ticker and save the results to a list
predictions = list(tqdm(p.imap(train_and_forecast, series), total=len(series)))
# Terminate the pool process
p.close()
# Tell the pool to wait till all the jobs are finished before exit
p.join()
# Concatenate results
multiprocess_forecast = pd.concat(predictions)
# Get the time used for the forecast
print('\nThe time used for the multi-processing forecast is ', time()-start_time)
#-------------------------------------------------------------#
# Step 7: Multiple Time Series Forecast Using Spark
#-------------------------------------------------------------#
# Import SparkSession
from pyspark.sql import SparkSession
# Create a Spark Session
spark = SparkSession.builder.master("local[*]").getOrCreate()
# Check Spark Session Information
spark
# Convert the pandas dataframe into a spark dataframe
sdf = spark.createDataFrame(df)
# Define the restult schema
result_schema =StructType([
  StructField('ds',DateType()),
  StructField('ticker',StringType()),
  StructField('yhat',FloatType()),
  StructField('yhat_upper',FloatType()),
  StructField('yhat_lower',FloatType())
  ])
# Start time
start_time = time()
# Train and forecast by ticker 
spark_forecast = sdf.groupBy('ticker').applyInPandas(train_and_forecast, schema=result_schema)
# Take a look at the results
spark_forecast.show(5)
# Processing time
print('The time used for the Spark forecast is ', time()-start_time)
###### Using Pandas UDF - It is preferred to use applyInPandas over this API because it will be deprecated in the future releases
@pandas_udf( result_schema, PandasUDFType.GROUPED_MAP )
def train_and_forecast(group):
  # Initiate the model
  m = Prophet()
  
  # Fit the model
  m.fit(group)
  # Make predictions
  future = m.make_future_dataframe(periods=15)
  forecast = m.predict(future)[['ds', 'yhat', 'yhat_lower', 'yhat_upper']]
  forecast['ticker'] = group['ticker'].iloc[0]
  
  # Return the forecasted results
  return forecast[['ds', 'ticker', 'yhat', 'yhat_upper', 'yhat_lower']]  
# Start time
start_time = time()
# Train and forecast by ticker 
spark_forecast = sdf.groupBy('ticker').apply(train_and_forecast)
# Take a look at the results
spark_forecast.show(15)
# Processing time
print('The time used for the Spark forecast is ', time()-start_time)