import yfinance as yf
# Prophet model for time series forecast
from prophet import Prophet
# Data processing
import numpy as np
import pandas as pd
# Visualization
import seaborn as sns
import matplotlib.pyplot as plt
# Model performance evaluation
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error
###### Step 2: Pull Data
# Data start date
start_date = '2011-01-02'
# Data end date. yfinance excludes the end date, so we need to add one day to the last day of data
end_date = '2022-10-12' 
# Date for splitting training and testing dataset
train_end_date = '2022-07-15'
# Pull close data from Yahoo Finance for the list of tickers
ticker_list = ['GOOG', 'VTI']
data = yf.download(ticker_list, start=start_date, end=end_date)[['Close']]
# Take a look at the data
data.head()
# Drop one level of the column names
data.columns = data.columns.droplevel(0)
# Take a look at the data
data.head()
# Information on the dataframe
data.info()
# Visualize data using seaborn
sns.set(rc={'figure.figsize':(12,8)})
sns.lineplot(x=data.index, y=data['GOOG'])
sns.lineplot(x=data.index, y=data['VTI'])
plt.legend(['Google', 'VTI'])
###### Step 3: Data Processing
# Change variable names
data = data.reset_index()
data.columns = ['ds', 'y', 'VTI']
# Take a look at the data
data.head()
# Check correlation
print (data["y"].corr(other=data["VTI"]))
###### Step 4: Train Test Split
# Train test split
train = data[data['ds'] <= train_end_date]
test = data[data['ds'] > train_end_date]
# Check the shape of the dataset
print(train.shape)
print(test.shape)
# Check the start and end time of the training and testing dataset
print('The start time of the training dataset is ', train['ds'].min())
print('The end time of the training dataset is ', train['ds'].max())
print('The start time of the testing dataset is ', test['ds'].min())
print('The end time of the testing dataset is ', test['ds'].max())
###### Step 5: Baseline Model
# Use the default hyperparameters to initiate the Prophet model
model_baseline = Prophet()
# Fit the model on the training dataset
model_baseline.add_regressor('VTI', standardize=False)
model_baseline.fit(train)
# Create the time range for the forecast
future_baseline = model_baseline.make_future_dataframe(periods=test.size.astype(int))
# Make prediction
forecast_baseline = model_baseline.predict(future_baseline)
# Visualize the forecast
model_baseline.plot(forecast_baseline); # Add semi-colon to remove the duplicated chart
# Visualize the forecast components
model_baseline.plot_components(forecast_baseline);
# Merge actual and predicted values
performance_baseline = pd.merge(test, forecast_baseline[['ds', 'yhat', 'yhat_lower', 'yhat_upper']][-test.size.astype(int):], on='ds')
# Check MAE value
performance_baseline_MAE = mean_absolute_error(performance_baseline['y'], performance_baseline['yhat'])
print(f'The MAE for the baseline model is {performance_baseline_MAE}')
plt.figure(figsize=(8,6))
plt.plot(performance_baseline['ds'], performance_baseline['y'], color="red", label='actual')
plt.plot(performance_baseline['ds'], performance_baseline['yhat'], color="blue", label='forecast')
plt.legend()

i=12