import vectorbt as vbt
import numpy as np
from itertools import combinations, product

symbols = ["SPY", "msft", "BIDU"]
price = vbt.YFData.download(symbols, missing_index='drop', start='2018-01-01 UTC').get('Close')

# Define hyper-parameter space
#rsi1_window, rsi2_window = vbt.utils.params.create_param_combs(
#    (product, (combinations, np.arange(3, 50, 1), 2)))

rsi_high, rsi_low = vbt.utils.params.create_param_combs( (product, (np.arange(3, 30, 1)), (np.arange(70, 97, 1) )))

rsi = vbt.RSI.run(price, 4, short_name="rsi1")
rqq = vbt.RSI.run(price, 16, short_name="rsi2")

entries = rsi.rsi_crossed_above(rsi_low)
exits = rqq.rsi_crossed_below(rsi_high)

pf_kwargs = dict(size=np.inf, fees=0.001, freq='1D')
pf = vbt.Portfolio.from_signals(price, entries, exits, short_entries=exits, short_exits=entries, **pf_kwargs)

print(pf.total_return())

fig = pf.total_return().vbt.heatmap(
    x_level='rsi1_window', y_level='rsi2_window', slider_level='symbol', symmetric=True,
    trace_kwargs=dict(colorbar=dict(title='Total return', tickformat='%')))
fig.show()
