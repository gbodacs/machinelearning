import vectorbt as vbt

import numpy as np
import pandas as pd
import itertools
from datetime import datetime, timedelta
import pytz
from numba import njit
import ipywidgets

seed = 42
symbols = [
    'HPE', 'ORCL', 'BIDU', 'MSFT', 'TLT'
]
start_date = datetime(2018, 11, 20, tzinfo=pytz.utc)
end_date = datetime(2022, 11, 20, tzinfo=pytz.utc)
time_delta = end_date - start_date
window_len = timedelta(days=180)
window_count = 400
exit_types = ['SL', 'TS', 'TP', 'Random', 'Holding']
step = 0.01  # 1%
stops = np.arange(step, 1 + step, step)

vbt.settings.array_wrapper['freq'] = 'd'
vbt.settings.plotting['layout']['template'] = 'vbt_dark'
vbt.settings.portfolio['init_cash'] = 100.  # 100$
vbt.settings.portfolio['fees'] = 0.0025  # 0.25%
vbt.settings.portfolio['slippage'] = 0.0025  # 0.25%

print(pd.Series({
    'Start date': start_date,
    'End date': end_date,
    'Time period (days)': time_delta.days,
    'Assets': len(symbols),
    'Window length': window_len,
    'Windows': window_count,
    'Exit types': len(exit_types),
    'Stop values': len(stops),
    'Tests per asset': window_count * len(stops) * len(exit_types),
    'Tests per window': len(symbols) * len(stops) * len(exit_types),
    'Tests per exit type': len(symbols) * window_count * len(stops),
    'Tests per stop type and value': len(symbols) * window_count,
    'Tests total': len(symbols) * window_count * len(stops) * len(exit_types)
}))

cols = ['Open', 'Low', 'High', 'Close', 'Volume']
yfdata = vbt.YFData.download(symbols, start=start_date, end=end_date)

print(yfdata.data.keys())
print(yfdata.data['MSFT'].shape)

yfdata.data['MSFT'].vbt.ohlcv.plot().show_svg()

i=22